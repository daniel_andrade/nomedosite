Sou estudante de Engenharia Mecatrônica na Escola Politécnica da Universidade de São Paulo. Atualmente, integro a Equipe Poli Racing, um grupo de extensão universitária que projeta e constrói carros de Fórmula SAE.

Na equipe, aplicamos o conhecimento adquirido em sala de aula para desenvolver um veículo competitivo para a Fórmula SAE, uma competição estudantil de engenharia automobilística.