---
title: "Unidade de Distribuição de Energia (PDU)"
imagem: "../images/pdu.png"
date: 2021-09-18T23:28:40-03:00
layout: "lista2"
draft: false
---

Unidade de Distribuição de Energia, ou Power Distribution Energy, ou simplismente PDU é um dos projetos mais novos do carro o qual tinha por objetivo alocar os componentes voltados para a alimentação do carro

Assim como o projeto do painel, este é feito de impressão 3D sendo necessário um arquivo CAD em stl para sua fabricação. Por passar toda a enegia do carro, a PDU exigi cabos com a seção transversal maior, garantindo um bom funcionamento e segurança.

PDU foi projetada visando ser uma peça independente do chicote, além de facilicar a manutençao e troca de alguns componente como fusivéis.