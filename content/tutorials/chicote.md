---
title: 'Chicote Elétrico'
imagem: "../images/Chicote.png"
date: 2024-10-11T09:38:06-03:00
layout: "lista"
draft: false
---

Um dos grandes projeto ao qual tive o prazer de participar foi o da manufatura de um chicote elétrico para o carro.

O chicote elétrica é toda o cabeamento do carro, sendo responsável por alimentar todos os componentes do carro, como sensores e motor, além de fazer a comunicação da ECU com o restante do carro.

Para o desenvolvimento desse projeto, utilizamos barbantes para retirar as medidas do chassi por onde o chicote iria passar. Em seguida, fim um diagrama multifilar que serve de mapa para conseguirmos fazer testes posteriores no chicote.