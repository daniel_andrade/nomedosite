---
title: "Painel"
imagem: "../images/painel.png"
date: 2021-09-18T23:28:40-03:00
layout: "lista3"
draft: false
---

O projeto do painel é o centro de comandos do carro, onde além de dar partida, controla as ventuinhas do carro, a injeção de conbustível e sistemas de leds para informar ao piloto certas condições como o neutro do carro.

No painel esta incluido uma dashboard que mostra informações de alguns sensores do carro para o piloto estar ciente em que estado esta o carro.

O painel é produzido por impressão 3D, por isso é necessário um desenho em stl para a sua impressão. Além disso é necessário confeccionar um chicote para seu funcionamento.