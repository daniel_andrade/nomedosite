---
title: "Biografia"
date: 2021-09-18T16:59:34-03:00
layout: "single"
draft: false
---

Nasci em 15 de fevereiro de 2004 e cresci na cidade de São Paulo, onde estudei até os 14 anos no colégio Veruska.

Aos 15 anos, ingressei na Escola Técnica Estadual Getúlio Vargas (ETEC GV) no curso técnico de Mecatrônica, iniciando minha trajetória na área de engenharia e desenvolvendo novas habilidades.

Em 2021, durante o terceiro ano do ensino médio, comecei a me preparar para os vestibulares, com o objetivo de ingressar na Escola Politécnica da Universidade de São Paulo (Poli-USP). Obtive sucesso nesse objetivo, o que marcou o início de uma nova e desafiadora etapa em minha vida acadêmica.

A faculdade me proporcionou um aprendizado prático por meio de experimentos, ensinando-me a utilizar equipamentos como o osciloscópio. Além disso, tive a oportunidade de desenvolver habilidades em mecânica, como a operação de tornos manuais e CNC.

O início da graduação foi desafiador, exigindo um período de adaptação à nova rotina, principalmente devido à distância da faculdade e ao alto nível de exigência do curso.

No segundo semestre, decidi participar do processo seletivo para integrar a Equipe Poli Racing (EPR), um grupo de extensão universitária que se dedica a projetar e manufaturar um carro de Fórmula SAE para competições estudantis contra outras universidades do Brasil.

Minha experiência na equipe tem sido enriquecedora, proporcionando-me a oportunidade de vivenciar a aplicação prática da engenharia e desenvolver habilidades em diversas áreas, desde a concepção e projeto até a manufatura e realização de um veículo, incluindo etapas como orçamento, contato com empresas e desenvolvimento de desenhos técnicos.

Em meu segundo ano na equipe, participei da minha primeira competição da Fórmula SAE, uma experiência memorável que me permitiu conhecer outras equipes, trocar conhecimentos e aprimorar o trabalho sob pressão. Além disso, a competição consolidou-se como um dos momentos mais marcantes da minha trajetória na equipe e na faculdade.